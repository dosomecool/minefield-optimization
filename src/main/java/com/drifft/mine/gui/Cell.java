package com.drifft.mine.gui;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.layout.Pane;

public class Cell extends Pane {

	private Controller controller; 

	private int xCoordinate; 
	private int yCoordinate;

	private double derivativeX;
	private double derivativeY;

	private boolean isMine = false; 
	private DoubleProperty effectiveness = new SimpleDoubleProperty(0.0);
	private List<Double> probabilityList = new ArrayList<>();

	public Cell(Controller controller, int x, int y) {
		this.controller = controller;
		this.xCoordinate = x;
		this.yCoordinate = y;
	
		double width = controller.getGridWidth().doubleValue() / controller.getColumns().doubleValue();
		double height = controller.getGridHeight().doubleValue() / controller.getRows().doubleValue();		
		this.setPrefSize(width, height);
		
		this.setStyle("-fx-border-color: red; "
				+ "-fx-background-color: #0A0AAA;"
				+ "-fx-background-radius: 0.0px;"
				+ "-fx-border-width: 0.0px;");

		setListeners();
	}

	public void evaluateEffectiveness() {
		double product = 1.0;

		for(Double p : probabilityList) {
			product = product * (1.0 - p); 
		}
		setEffectiveness(1-product);
	}
		
	public void clearProbabilityList() {
		probabilityList.clear();
	}

	public void setListeners() {
		
		this.setOnMouseClicked( e -> {
			this.setStyle("-fx-background-color: black;");
			controller.getGrid().getMineList().add(this);
		});

		effectiveness.addListener( (observable, oldValue, newValue) -> {

			if(newValue.doubleValue() > 0.0 && newValue.doubleValue() <= 0.1) {
				this.setStyle("-fx-background-color: #022CDD;");
			} else if(newValue.doubleValue() > 0.1 && newValue.doubleValue() <= 0.2) {
				this.setStyle("-fx-background-color: #0196FB;");
			} else if(newValue.doubleValue() > 0.2 && newValue.doubleValue() <= 0.3) {
				this.setStyle("-fx-background-color: #01ECFD;");
			} else if(newValue.doubleValue() > 0.3 && newValue.doubleValue() <= 0.4) {
				this.setStyle("-fx-background-color: #17F8AF;");
			} else if(newValue.doubleValue() > 0.4 && newValue.doubleValue() <= 0.5) {
				this.setStyle("-fx-background-color: #9FFB5A;");
			} else if(newValue.doubleValue() > 0.5 && newValue.doubleValue() <= 0.6) {
				this.setStyle("-fx-background-color: #ffd400;");
			} else if(newValue.doubleValue() > 0.6 && newValue.doubleValue() <= 0.7) {
				this.setStyle("-fx-background-color: #ffae00;");
			} else if(newValue.doubleValue() > 0.7 && newValue.doubleValue() <= 0.8) {
				this.setStyle("-fx-background-color: #FF4500;");
			} else if(newValue.doubleValue() > 0.8 && newValue.doubleValue() <= 0.9) {
				this.setStyle("-fx-background-color: #FF1901;");
			} else if(newValue.doubleValue() > 0.9 && newValue.doubleValue() <= 1.0) {
				this.setStyle("-fx-background-color: #ff0000;");
			} else {
				setStyle("-fx-background-color: #0A0AAA;");
			}
		});
	}

	public int getX() {
		return xCoordinate;
	}

	public int getY() {
		return yCoordinate;
	}

	public Double getEffectiveness() {
		return effectiveness.getValue();
	}

	public void setEffectiveness(Double effectiveness) {
		this.effectiveness.set(effectiveness);
	}

	public List<Double> getProbabilityList() {
		return probabilityList;
	}

	public double getDerivativeX() {
		return derivativeX;
	}

	public void setDerivativeX(double derivativeX) {
		this.derivativeX = derivativeX;
	}

	public double getDerivativeY() {
		return derivativeY;
	}

	public void setDerivativeY(double derivativeY) {
		this.derivativeY = derivativeY;
	}
}
