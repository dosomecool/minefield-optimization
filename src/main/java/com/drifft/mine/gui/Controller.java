package com.drifft.mine.gui;

import java.util.List;

import com.drifft.mine.gui.Menu.AnalysisType;
import com.drifft.mine.minefield.OptimizeMinefield;
import com.drifft.mine.minefield.PathwiseSurvivalAnalysis;
import com.drifft.mine.minefield.SimpleSurvivalAnalysis;

public class Controller {

	private final static Integer GRID_WIDTH = 500;
	private final static Integer GRID_HEIGHT = 500;

	private final static Integer MAX_COLUMNS = 50;
	private final static Integer MAX_ROWS = 50; 

	private Root rootPane; 

	private Grid grid;
	private MiddlePane middlePane;
	private RightPane rightPane; 
	private Menu menu; 

	private SimpleSurvivalAnalysis simpleSurvivalAnalysis;
	private PathwiseSurvivalAnalysis pathwiseSurvivalAnalysis;

	int minefieldIndex = 1; 
	
	private OptimizeMinefield optimizer;

	public Controller() {

		this.rootPane = new Root(this); 

		this.menu = new Menu(this);
		this.rootPane.setTop(this.menu);

		this.grid = new Grid(this);
		this.rootPane.getLeftVBox().getChildren().add(this.grid);

		this.middlePane = new MiddlePane(this); 
		this.rootPane.getMiddleVBox().getChildren().add(this.middlePane);

		this.rightPane = new RightPane(this); 
		this.rootPane.getRightVBox().getChildren().add(this.rightPane);

		this.optimizer = new OptimizeMinefield(this); 
	}

	public void evaluateSurvival() {
		AnalysisType analysisType = this.menu.getAnalysisType();
		
		List<Double> expectedPmf;
		List<Double> expectedCdf;
		List<Double> expectedSurvival;
		List<Double> expectedHazard;

		switch(analysisType) {

		case SIMPLE:
			simpleSurvivalAnalysis = new SimpleSurvivalAnalysis(this);
			simpleSurvivalAnalysis.evaluateSurvival();
			middlePane.plot(simpleSurvivalAnalysis);
			
			expectedPmf  = simpleSurvivalAnalysis.getExpectedPmf();
			expectedCdf  = simpleSurvivalAnalysis.getExpectedCdf();
			expectedSurvival = simpleSurvivalAnalysis.getExpectedSurvival(); 
			expectedHazard  = simpleSurvivalAnalysis.getExpectedHazard();

			if(minefieldIndex <=10) {
				rightPane.getExpectedPmfChart().addSeries("pmf" + String.valueOf(minefieldIndex), expectedPmf);
				rightPane.getExpectedCdfChart().addSeries("cdf" + String.valueOf(minefieldIndex), expectedCdf);
				rightPane.getExpectedSurvivalChart().addSeries("survival" + String.valueOf(minefieldIndex), expectedSurvival);
				rightPane.getExpectedHazardChart().addSeries("hazard" + String.valueOf(minefieldIndex), expectedHazard);
				grid.getMinefieldVBox().getChildren().add(( minefieldIndex-1 ), new MinefieldRow(minefieldIndex, simpleSurvivalAnalysis)); 
				minefieldIndex = minefieldIndex + 1;
			}
			break;

		case PATHWISE:
			pathwiseSurvivalAnalysis = new PathwiseSurvivalAnalysis(this);
			pathwiseSurvivalAnalysis.evaluateSurvival();
			middlePane.plot(pathwiseSurvivalAnalysis);	
			
			expectedPmf = pathwiseSurvivalAnalysis.getExpectedPmf();
			expectedCdf = pathwiseSurvivalAnalysis.getExpectedCdf();
			expectedSurvival = pathwiseSurvivalAnalysis.getExpectedSurvival(); 
			expectedHazard  = pathwiseSurvivalAnalysis.getExpectedHazard();

			if(minefieldIndex <=10) {
				rightPane.getExpectedPmfChart().addSeries("pmf" + String.valueOf(minefieldIndex), expectedPmf);
				rightPane.getExpectedCdfChart().addSeries("cdf" + String.valueOf(minefieldIndex), expectedCdf);
				rightPane.getExpectedSurvivalChart().addSeries("survival" + String.valueOf(minefieldIndex), expectedSurvival);
				rightPane.getExpectedHazardChart().addSeries("hazard" + String.valueOf(minefieldIndex), expectedHazard);
				grid.getMinefieldVBox().getChildren().add(( minefieldIndex-1 ), new MinefieldRow(minefieldIndex, pathwiseSurvivalAnalysis));  
				minefieldIndex = minefieldIndex + 1;
			}
			
			grid.drawTransitDistribution(pathwiseSurvivalAnalysis.getMean(), pathwiseSurvivalAnalysis.getSd());
			break;
		}
	}

	// Getters and Setters

	public Integer getGridWidth() {
		return GRID_WIDTH; 
	}

	public Integer getGridHeight() {
		return GRID_HEIGHT; 
	}

	public Integer getRows() {
		return MAX_ROWS;
	}

	public Integer getColumns() {
		return MAX_COLUMNS;
	}

	public Root getRootPane() {
		return rootPane;
	}

	public Grid getGrid() {
		return grid;
	}

	public MiddlePane getChartPane() {
		return middlePane;
	}

	public Menu getMenu() {
		return menu;
	}

	public SimpleSurvivalAnalysis getSimpleSurvivalAnalysis() {
		return simpleSurvivalAnalysis;
	}

	public PathwiseSurvivalAnalysis getPathwiseSurvivalAnalysis() {
		return pathwiseSurvivalAnalysis;
	}

	public OptimizeMinefield getOptimizer() {
		return optimizer;
	}
}
