package com.drifft.mine.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.drifft.mine.gui.Menu.AnalysisType;
import com.drifft.mine.minefield.Circle;
import com.drifft.mine.minefield.Point;
import com.jfoenix.controls.JFXCheckBox;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;

public class Grid extends VBox {

	@FXML
	private VBox rootVBox;

	@FXML
	private Label title;
	
    @FXML
    private Label statLabel;

	@FXML
	private JFXCheckBox drawCheckBox;

	@FXML
	private StackPane layersStackPane;

	@FXML
	private TilePane effectivenessTilePane;

	@FXML
	private TilePane transitorTilePane;

	@FXML
	private HBox distributionHBox;

	@FXML
	private VBox minefieldVBox;

	private List<Cell> mineList = new ArrayList<Cell>();

	double mean = 0.0;
	double variance = 0.0;

	private Circle circle = new Circle();

	double[][] effectiveness;
	double[][] transitorProbability; 

	private Controller controller; 

	public Grid(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("LeftPane.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {
		effectiveness = new double[controller.getColumns()][controller.getRows()]; 
		transitorProbability = new double[controller.getColumns()][controller.getRows()]; 

		effectivenessTilePane.toFront();

		int rowIndex = controller.getColumns() - 1;
		int columns = controller.getRows(); 

		for(int y=rowIndex; y>=0; y--) {

			for(int x=0; x<columns; x++) {
				effectivenessTilePane.getChildren().add(new Cell(controller, x, y));
				transitorTilePane.getChildren().add(new TransitorCell(controller, x, y));
				effectiveness[x][y] = 0.0;
			}
		}

		drawCheckBox.setOnAction(e -> {

			AnalysisType analysisType = controller.getMenu().getAnalysisType();

			if(drawCheckBox.isSelected() && analysisType == AnalysisType.PATHWISE) {
				controller.getGrid().getTransitorTilePane().getChildren().forEach( transitorCell -> {
					((TransitorCell) transitorCell).addDrawHandler(); 
				});

			} else if(!drawCheckBox.isSelected()) {
				controller.getGrid().getTransitorTilePane().getChildren().forEach( transitorCell -> {
					((TransitorCell) transitorCell).removeDrawHandler(); 
				});
			}
		});
	}

	public Cell getCellByCoordinate(final int x, final int y) {

		for(Node cell : effectivenessTilePane.getChildren()) {

			Cell point = (Cell) cell; 
			if(point.getX() == x && point.getY() == y) {
				return point;
			}
		}
		return null;
	}

	public TransitorCell getTransitorCellByCoordinate(int x, int y) {

		for(Node transitorCell : transitorTilePane.getChildren()) {

			TransitorCell point = (TransitorCell) transitorCell; 
			if(point.getX() == x && point.getY() == y) {
				return point;
			}
		}
		return null;
	}

	public void highlightCell(Cell cell) {
		cell.setStyle("-fx-background-color: #FFFFFF;");
	}

	public Cell getCellWithCoordinate(int x, int y) {

		for(Node cell : effectivenessTilePane.getChildren()) {

			int xPosition = ((Cell) cell).getX();
			int yPosition = ((Cell) cell).getY();

			if(x == xPosition && y == yPosition) {
				return ((Cell) cell);
			}
		}
		return null;
	}

	public void evaluate() {

		effectivenessTilePane.getChildren().forEach( cell -> {

			((Cell) cell).clearProbabilityList();

			mineList.forEach( mine -> {

				int x = mine.getX(); 
				int y = mine.getY();

				int cellX = ((Cell) cell).getX(); 
				int cellY = ((Cell) cell).getY();

				double distance = Math.sqrt( Math.pow(x - cellX, 2) + Math.pow(y - cellY, 2));

				if(distance <= circle.getRadius() ) {
					((Cell) cell).getProbabilityList().add(circle.getProbability(distance));
				}
			});

			((Cell) cell).evaluateEffectiveness();
			int x = ((Cell) cell).getX();
			int y = ((Cell) cell).getY();
			effectiveness[x][y] = ((Cell) cell).getEffectiveness().doubleValue(); 
		});

		mean = (double) Math.round(calculateMean() * 100d) / 100d;
		variance = (double) Math.round(calculateVariance() * 100d) / 100d;
		statLabel.setText("MEAN: " + String.valueOf(mean) + " VAR: " + String.valueOf(variance));
	}


	public double calculateMean() {
		double sum = 0.0;

		for(Node cell : effectivenessTilePane.getChildren()) {
			// mean 
			sum = sum + ((Cell) cell).getEffectiveness();
		}
		return mean = sum / effectivenessTilePane.getChildren().size(); 
	}

	public double calculateVariance() {
		double sum = 0.0;

		for(Node cell : effectivenessTilePane.getChildren()) {
			// mean 
			sum = sum + Math.pow(( ((Cell) cell).getEffectiveness() - mean ), 2);
		}
		return variance = sum / effectivenessTilePane.getChildren().size(); 
	}

	public void clearMinefield() {		

		effectivenessTilePane.getChildren().forEach( cell -> {
			((Cell) cell).setEffectiveness(0.0);

			int x = ((Cell) cell).getX();
			int y = ((Cell) cell).getY();
			effectiveness[x][y] = 0.0;
		});

		mean = 0.0;
		variance = 0.0;
	}

	public void clearTransit() {		
		transitorTilePane.getChildren().forEach( transitorCell -> {
			((TransitorCell) transitorCell).clearTransit();
		});
	}

	public List<Point> getTransitPath() {
		List<Point> points = new ArrayList<>();

		for(int y=0; y<controller.getRows(); y++) {

			for(int x=0; x<controller.getColumns(); x++) {
				TransitorCell  transitorCell = getTransitorCellByCoordinate(x, y);

				if(transitorCell.getCount() >= 1) {
					int xj = transitorCell.getX();
					int yj = transitorCell.getY();
					points.add(new Point(xj, yj));
				}	
			}
		}
		return points; 	
	}

	public void calculateTransitProbability() {

		for(int y=0; y<controller.getRows(); y++) {
			Integer sum = 0;

			for(int x=0; x<controller.getColumns(); x++) {
				sum = sum + getTransitorCellByCoordinate(x, y).getCount();
			}

			for(int x=0; x<controller.getColumns(); x++) {
				TransitorCell transitorCell = getTransitorCellByCoordinate(x, y);
				Integer count = transitorCell.getCount();  
				double probability = count.doubleValue() / sum.doubleValue();
				transitorProbability[x][y] = probability;
			}
		}
	}

	public void setTransitProbability(double[][] probability) {

		for(int y=0; y<controller.getRows(); y++) {

			for(int x=0; x<controller.getColumns(); x++) {
				TransitorCell transitorCell = getTransitorCellByCoordinate(x, y);
				transitorCell.setTransitProbability(probability[x][y]);
			}	
		}
	}

	public void drawTransitDistribution(int[] mean, int[] sd) {

		for(int j=0; j<controller.getRows(); j++) {

			int x = mean[j];
			int v = sd[j];
			int y = j;

			try {
				TransitorCell mCell = getTransitorCellByCoordinate(x, y);
				mCell.highlightCell("#00FF00"); 
				
			} catch(NullPointerException e) {
				// Some points may fall outsude
			}
			
			try {
				TransitorCell vPlus = getTransitorCellByCoordinate(x + v, y);
				vPlus.highlightCell("#FF1493");  
				
			} catch(NullPointerException e) {
				// Some points may fall outsude
			}
			
			try {
				TransitorCell vMinus = getTransitorCellByCoordinate(x - v, y);
				vMinus.highlightCell("#FF1493");  
				
			} catch(NullPointerException e) {
				// Some points may fall outsude
			}
		}
	}



	public void addRectangle(double[] pmf) {
		distributionHBox.getChildren().clear();
		int bins = pmf.length;
		double height = distributionHBox.getHeight();
		double width = (effectivenessTilePane.getWidth() - 100) / controller.getColumns(); 

		for(int i=1; i<=bins; i++) {
			Rectangle r = new Rectangle();
			r.setWidth( width );
			r.setHeight( height * pmf[i-1] );
			r.setArcWidth(5);
			r.setArcHeight(5);
			r.setStyle("-fx-stroke: #FF1901; "
					+ "-fx-fill: #FF1901;");
			distributionHBox.getChildren().add(r);
		}
	}

	// Getters and Setters

	public VBox getRootVBox() {
		return rootVBox;
	}

	public StackPane getLayersStackPane() {
		return layersStackPane;
	}

	public VBox getMinefieldVBox() {
		return minefieldVBox;
	}

	public List<Cell> getMineList() {
		return mineList;
	}

	public void setMineList(List<Cell> mineList) {
		this.mineList = mineList;
	}	

	public double getMean() {
		return mean;
	}

	public double getVariance() {
		return variance;
	}

	public TilePane getEffectivenessTilePane() {
		return effectivenessTilePane;
	}

	public TilePane getTransitorTilePane() {
		return transitorTilePane;
	}

	public HBox getDistributionHBox() {
		return distributionHBox;
	}

	public Circle getCircle() {
		return circle;
	}

	public double[][] getEffectiveness() {
		return effectiveness;
	}

	public double[][] getTransitorProbability() {
		return transitorProbability;
	}
}
