package com.drifft.mine.gui;

import java.io.IOException;

import com.drifft.mine.utilities.Statistics;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class Menu extends AnchorPane {

	@FXML
	private JFXComboBox<AnalysisType> analysisTypeComboBox;

	@FXML
	private JFXComboBox<InitialPosition> initialPositionComboBox;

	@FXML
	private JFXComboBox<Integer> optionAComboBox;

	@FXML
	private JFXComboBox<Uncertanty> uncertantyComboBox;

	@FXML
	private JFXComboBox<LayerType> topLayerComboBox;

	@FXML
	private JFXButton evaluateEffectivenessButton;

	@FXML
	private JFXButton survivalButton;

	@FXML
	private JFXButton clearButton;

	@FXML
	private JFXButton clearTransitButton;

	@FXML
	private JFXButton optimizeButton;

	private int position = 0;
	private int width = 0;  
	private int variance = 0;

	Controller controller;

	public Menu(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Menu.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {

		evaluateEffectivenessButton.setOnAction( e -> {
			controller.getGrid().evaluate();
		});

		clearButton.setOnAction( e -> {
			controller.getGrid().clearMinefield();
			controller.getGrid().getMineList().clear();
			controller.getGrid().evaluate();
		});

		survivalButton.setOnAction( e -> {
			controller.evaluateSurvival(); 
		});

		// Layer Type
		for (LayerType type : LayerType.values()) {
			topLayerComboBox.getItems().add(type);
		}
		topLayerComboBox.setValue(LayerType.EFFECTIVENESS);

		// listener
		topLayerComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {

			if(newValue != null) {

				switch(newValue) {
				case EFFECTIVENESS:
					controller.getGrid().getEffectivenessTilePane().toFront();
					break;
				case TRANSITOR: 
					controller.getGrid().getTransitorTilePane().toFront();
					break;
				default:
					break;
				}
			}
		});

		// Initial Position
		for (InitialPosition type : InitialPosition.values()) {
			initialPositionComboBox.getItems().add(type);
		}

		initialPositionComboBox.setValue(InitialPosition.POINT);
		initialPositionComboBox.getSelectionModel().selectFirst();
		initialPositionComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {

			if(newValue != null) {

				switch(newValue) {

				case POINT: 
					optionAComboBox.setPromptText("");
					optionAComboBox.setDisable(true);
					break;

				case UNIFORM: 

					for(int i=0; i<=controller.getColumns(); i++) {
						optionAComboBox.getItems().add(i);
					}
					optionAComboBox.setPromptText("WIDTH");
					optionAComboBox.setDisable(false);
					break;

				case GAUSSIAN: 

					for(int i=0; i<=controller.getColumns(); i++) {
						optionAComboBox.getItems().add(i);
					}
					optionAComboBox.setPromptText("VARIANCE");
					optionAComboBox.setDisable(false);
					break; 
				}
			}
		});
		
		initialPositionComboBox.disabledProperty().addListener((observable, oldValue, newValue) -> {
			
			if(newValue == true) {
				optionAComboBox.setDisable(true);
			} else {
				optionAComboBox.setDisable(false);
			}
		});

		optionAComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {

			if(newValue != null) {

				if(optionAComboBox.getPromptText().equals("POSITION")) {
					position = newValue;
					addInitialPosition(); 
				
				} else if(optionAComboBox.getPromptText().equals("WIDTH")) {
					width = newValue;
					addUniformInitialDistribution();
					
				} else if(optionAComboBox.getPromptText().equals("VARIANCE")) {
					variance = newValue;
					addGaussianInitialDistribution();
				}
			}
		});

		// Uncertanty
		for (Uncertanty type : Uncertanty.values()) {
			uncertantyComboBox.getItems().add(type);
		}

		uncertantyComboBox.setValue(Uncertanty.NONE);
		uncertantyComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {

			if(newValue != null) {

				switch(newValue) {
				case NONE:
					initialPositionComboBox.setDisable(true);
					break;

				case TIME_DEPENDENT:
					initialPositionComboBox.setDisable(false);
					break;

				default:
					break;
				}
			}
		});

		optimizeButton.setOnAction( e -> {
			controller.getOptimizer().optimize();
			controller.getGrid().evaluate();
		});


		// Analysis Type
		for (AnalysisType type : AnalysisType.values()) {
			analysisTypeComboBox.getItems().add(type);
		}
		analysisTypeComboBox.setValue(AnalysisType.SIMPLE);

		analysisTypeComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {

			if(newValue != null) {

				switch(newValue) {

				case SIMPLE:
					initialPositionComboBox.setDisable(true);
					uncertantyComboBox.setDisable(true);
					break;

				case PATHWISE:
					initialPositionComboBox.setDisable(false);
					uncertantyComboBox.setDisable(false);
					optionAComboBox.setDisable(true);
					break;

				default:
					break;
				}
			}
		});
		
		clearTransitButton.setOnAction(e -> {
			controller.getGrid().clearTransit();
		});
	}
	
	public void addInitialPosition() {
		int bins     = controller.getColumns();
		double[] pmf = Statistics.getUniformPMF(26, 0, bins); 
		controller.getGrid().addRectangle(pmf);
	}

	private void addUniformInitialDistribution() {
		int bins     = controller.getColumns();
		int x0       = controller.getColumns() / 2;
		double[] pmf = Statistics.getUniformPMF(x0, width, bins); 
		controller.getGrid().addRectangle(pmf);
	}

	private void addGaussianInitialDistribution() {
		int bins     = controller.getColumns();
		int x0       = controller.getColumns() / 2;
		double[] pmf = Statistics.getGaussianPMF(x0, variance, bins); 		
		controller.getGrid().addRectangle(pmf);
	}

	public enum AnalysisType {	
		SIMPLE, PATHWISE; 
	} 

	public enum LayerType {	
		EFFECTIVENESS, TRANSITOR; 
	} 

	public enum InitialPosition {	
		POINT, UNIFORM, GAUSSIAN;
	}

	public enum Uncertanty {
		NONE, TIME_DEPENDENT; 
	}

	// Getters and Setters

	public AnalysisType getAnalysisType() {
		return analysisTypeComboBox.getValue(); 
	}

	public InitialPosition getInitialPosition() {
		return initialPositionComboBox.getValue(); 
	}

	public Uncertanty getUncertanty() {
		return uncertantyComboBox.getValue(); 
	}
	
	public int getPosition() {
		return position;
	}

	public int getDistributionWidth() {
		return width;
	}

	public int getVariance() {
		return variance;
	}
}
