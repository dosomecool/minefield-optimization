package com.drifft.mine.gui;

import java.io.IOException;

import com.drifft.mine.minefield.Plottable;

import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

// Source: https://www.techyourchance.com/thread-safe-observer-design-pattern-in-java/

public class MiddlePane extends VBox {

    @FXML
    private Label label;

    @FXML
    private VBox chartContainer;

	private Controller controller;

	// Charts
	private SurfaceChart lifetimePmfChart;
	private SurfaceChart lifetimeCdfChart;
	private SurfaceChart survivalChart;
	private SurfaceChart hazardChart;

	public MiddlePane(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("MiddlePane.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void initialize() {
		lifetimePmfChart = new SurfaceChart(controller, "pmf.html", "LIFETIME PMF");
		lifetimeCdfChart = new SurfaceChart(controller, "cdf.html", "LIFETIME CDF");
		survivalChart = new SurfaceChart(controller, "survival.html", "SURVIVAL FUNCTION"); 
		hazardChart = new SurfaceChart(controller, "hazard.html", "HAZARD FUNCTION"); 
		
		chartContainer.getChildren().add(lifetimePmfChart);
		chartContainer.getChildren().add(lifetimeCdfChart);
		chartContainer.getChildren().add(survivalChart);
		chartContainer.getChildren().add(hazardChart);
	
	}
	
	public void swap(Node node) {
		final Node targetNode = node;
		FadeTransition fadeOut = new FadeTransition(Duration.millis(500), targetNode);
		FadeTransition fadein = new FadeTransition(Duration.millis(200), targetNode);

		fadeOut.setFromValue(1.0);
		fadeOut.setToValue(0.20);
		fadeOut.setCycleCount(1);
		fadeOut.setOnFinished( e -> {
			fadein.play();
			chartContainer.getChildren().remove(targetNode);
			chartContainer.getChildren().add(0, targetNode);		
		});
		fadein.setFromValue(0.20);
		fadein.setToValue(1.0);
		fadein.setCycleCount(1);
		fadeOut.play();
	}

	public void setTitle(String title) {
		this.label.setText(title);		
	}

	public void plot(Plottable survivalAnalysis) {		
		lifetimePmfChart .setData(survivalAnalysis.getPmfData());
		lifetimeCdfChart.setData(survivalAnalysis.getCdfData());
		survivalChart.setData(survivalAnalysis.getSurvivalData()); 
		hazardChart.setData(survivalAnalysis.getHazardData());
	}

	// *** Getters / Setters ***

}
