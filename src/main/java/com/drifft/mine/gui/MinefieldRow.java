package com.drifft.mine.gui;

import java.io.IOException;
import java.util.List;

import com.drifft.mine.minefield.SurvivalAnalysis;
import com.jfoenix.controls.JFXCheckBox;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;

public class MinefieldRow extends HBox {

    @FXML
    private Region colorRegion;

    @FXML
    private Label numberLabel;

    @FXML
    private Label meanLabel;

    @FXML
    private Label varianceLabel;

    @FXML
    private Label expectedLifetimeLabel;

    @FXML
    private JFXCheckBox selectCheckBox;

	int minefieldIndex = 0;
	double expectedLifetime = 0.0;
	double mean = 0.0;
	double variance = 0.0;
	
	private List<Double> expectedSurvival; 

	public MinefieldRow(int minefieldIndex, SurvivalAnalysis survivalAnalysis) {
		this.minefieldIndex = minefieldIndex; 
		this.expectedLifetime = Math.floor(survivalAnalysis.getExpectedLifetime() * 100) / 100;
		this.expectedSurvival = survivalAnalysis.getExpectedSurvival(); 
		this.mean = survivalAnalysis.getMinefieldMean();
		this.variance = survivalAnalysis.getMinefieldVariance();
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("MinefieldRow.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {

		if(minefieldIndex == 1) {
			colorRegion.setStyle("-fx-background-color: #ffae00;");
		} else if(minefieldIndex == 2) {
			colorRegion.setStyle("-fx-background-color: #FF1901;");
		} else if(minefieldIndex == 3) {
			colorRegion.setStyle("-fx-background-color: #9FFB5A;");
		} else if(minefieldIndex == 4) {
			colorRegion.setStyle("-fx-background-color: #17F8AF;");
		} else if(minefieldIndex == 5) {
			colorRegion.setStyle("-fx-background-color: #01ECFD;");
		} else if(minefieldIndex == 6) {
			colorRegion.setStyle("-fx-background-color: #0196FB;");
		} else if(minefieldIndex == 7) {
			colorRegion.setStyle("-fx-background-color: #014BFF;");
		} else if(minefieldIndex == 8) {
			colorRegion.setStyle("-fx-background-color: #02178D;");
		} else if(minefieldIndex == 9) {
			colorRegion.setStyle("-fx-background-color: #022CDD;");
		} else if(minefieldIndex == 10) {
			colorRegion.setStyle("-fx-background-color: #FF4500;");
		} 

		numberLabel.setText(String.valueOf(minefieldIndex));
		meanLabel.setText(String.valueOf(mean));
		varianceLabel.setText(String.valueOf(variance));
		expectedLifetimeLabel.setText(String.valueOf(expectedLifetime));
	}

	public List<Double> getExpectedSurvival() {
		return expectedSurvival;
	}
}
