package com.drifft.mine.gui;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class RightPane extends VBox {

    @FXML
    private Label label;

    @FXML
    private VBox chartContainer;
    
    private SurvivalLineChart expectedPmfChart;
    private SurvivalLineChart expectedCdfChart;
    private SurvivalLineChart expectedSurvivalChart;
    private SurvivalLineChart expectedHazardChart;
    
    private Controller controller;
    
	public RightPane(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("RightPane.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void initialize() {
		expectedPmfChart      = new SurvivalLineChart(); 
		expectedCdfChart      = new SurvivalLineChart(); 
		expectedSurvivalChart = new SurvivalLineChart(); 
		expectedHazardChart   = new SurvivalLineChart(); 
		chartContainer.getChildren().addAll(expectedPmfChart.getChart(),
				                     expectedCdfChart.getChart(),
				                     expectedSurvivalChart.getChart(),
				                     expectedHazardChart.getChart());
	}

	public SurvivalLineChart getExpectedPmfChart() {
		return expectedPmfChart;
	}

	public SurvivalLineChart getExpectedCdfChart() {
		return expectedCdfChart;
	}
	
	public SurvivalLineChart getExpectedSurvivalChart() {
		return expectedSurvivalChart;
	}

	public SurvivalLineChart getExpectedHazardChart() {
		return expectedHazardChart;
	}

	public VBox getChartContainer() {
		return chartContainer;
	}
}
