package com.drifft.mine.gui;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class Root extends BorderPane {

    @FXML
    private BorderPane root;

    @FXML
    private VBox centerVBox;

    @FXML
    private AnchorPane leftAnchorPane;

    @FXML
    private VBox leftVBox;

    @FXML
    private AnchorPane middleAnchorPane;

    @FXML
    private VBox middleVBox;

    @FXML
    private AnchorPane middleAnchorPane1;

    @FXML
    private VBox rightVBox;
    
	private Controller controller; 
	
	public Root(Controller controller) {
		this.controller = controller;
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Root.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.getStylesheets().addAll(getClass().getResource("DrifftStylesheet.css").toExternalForm());
	}

	@FXML
	public void initialize() {
	}
	
	// Getters and Setters.
		
	public VBox getLeftVBox() {
		return leftVBox;
	}
	
	public VBox getMiddleVBox() {
		return middleVBox;
	}
	
	public VBox getRightVBox() {
		return rightVBox;
	}
}
