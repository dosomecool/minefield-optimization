package com.drifft.mine.gui;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

import com.drifft.mine.minefield.Data;
import com.drifft.mine.minefield.DataPoint;

import javafx.concurrent.Worker.State;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.CacheHint;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

// Source: https://www.techyourchance.com/thread-safe-observer-design-pattern-in-java/
// source: https://riptutorial.com/javafx/example/19313/communication-between-java-app-and-javascript-in-the-web-page
// another good source: http://tutorials.jenkov.com/javafx/webview.html

public class SurfaceChart extends AnchorPane {

    @FXML
    private AnchorPane rootAnchorPane;

    @FXML
    private Label titleLabel;

    @FXML
    private WebView webView;
    
	private Controller controller;
    private JSObject javascriptLink;
    
    private String htmlName;
    private String title; 
   
	public SurfaceChart(Controller controller, String htmlName, String title) {
		this.controller = controller;
		this.htmlName = htmlName; 
		this.title = title; 

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("WebChart.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void initialize() {

		titleLabel.setText(title);
		
		webView.setCache(true);
		webView.setCacheHint(CacheHint.SPEED);
		
        // Create the WebEngine
        final WebEngine webEngine = webView.getEngine();
 
        // LOad the Start-Page
        URL url = this.getClass().getResource("/com/drifft/mine/gui/html/" + htmlName);
        webEngine.load(url.toString());
                         
        // set up the listener
        webEngine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
            
            if (newValue == State.SUCCEEDED) {
                javascriptLink = (JSObject) webEngine.executeScript("getLink()");
                
                Data data = new Data();
                data.addPoint(new DataPoint(0, 0, 0, 0, 0));
                setData(data); 
            }
        });
	}
	
    public void setData(Data data) { 
        javascriptLink.call("graph", Arrays.toString(data.getData().toArray()));
    }
    
    // *** Getters / Setters ***
    
	public String getHtmlName() {
		return htmlName;
	}

	public void setHtmlName(String htmlName) {
		this.htmlName = htmlName;
	}
}
