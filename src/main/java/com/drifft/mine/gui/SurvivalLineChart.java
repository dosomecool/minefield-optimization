package com.drifft.mine.gui;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

public class SurvivalLineChart {

	// Size
	private final static double WIDTH = 400;
	private final static double HEIGHT = 300;

	// Defining the axes
	final NumberAxis xAxis = new NumberAxis();
	final NumberAxis yAxis = new NumberAxis();
	
	// Line chart
	private ObservableList<XYChart.Series<Integer, Double>> lineChartData = FXCollections.observableArrayList();
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private final LineChart lineChart = new LineChart(xAxis, yAxis, lineChartData);

	// Data queue
	//	private BlockingQueue<Data> queue;

	public SurvivalLineChart() {

		// x-axis
		xAxis.setLabel("TIME STEP");
		xAxis.setMinorTickVisible(false);
		xAxis.setLowerBound(0);
		xAxis.setAutoRanging(true);

		// y-axis
		yAxis.setLabel("Probability S(t)");		
		yAxis.setMinorTickVisible(false);
		yAxis.setMinorTickCount(10);
		yAxis.setUpperBound(1.0);

		// Set chart properties		
		lineChart.setAnimated(false);
		lineChart.setLegendVisible(false);
		lineChart.setCreateSymbols(false);

		lineChart.setMinSize(WIDTH, HEIGHT);
		lineChart.setMaxSize(WIDTH, HEIGHT);

		lineChart.setCache(true);
	}

	public void addSeries(String seriesName, List<Double> dataList) {
		
		if(dataList.size() == 0 || dataList == null) {
			throw new IllegalStateException("Chart data does not exist!");
		}

		ObservableList<XYChart.Data<Integer, Double>> observableDataList = FXCollections.observableArrayList();
		
		for(int i=0; i<dataList.size(); i++) {
			observableDataList.add(new XYChart.Data<Integer, Double>(i, dataList.get(i))); 
		}
		
		Series<Integer, Double> series = new LineChart.Series<Integer, Double>(seriesName, observableDataList);
		this.lineChartData.add(series);
	}
	
	// add a map here
	public void addData(String seriesName, final double x, final double y) {
//		series.getData().add(new XYChart.Data<Double, Double>(x, y));
	}

	// *** Getters / Setters ***

	@SuppressWarnings("rawtypes")
	public LineChart getChart() {
		return lineChart;
	}
}
