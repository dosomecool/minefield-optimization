package com.drifft.mine.gui;

import com.drifft.mine.minefield.Point;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class TransitorCell extends Pane {

	private int xCoordinate; 
	private int yCoordinate;

	private Integer count = 0;
	private DoubleProperty transitProbability = new SimpleDoubleProperty(0.0);

	Controller controller;

	public TransitorCell(Controller controller, int x, int y) {
		this.controller = controller; 
		this.xCoordinate = x;
		this.yCoordinate = y;

		double width = controller.getGridWidth().doubleValue() / controller.getColumns().doubleValue();
		double height = controller.getGridHeight().doubleValue() / controller.getRows().doubleValue();		
		this.setPrefSize(width, height);

		this.setStyle("-fx-border-color: red; "
				+ "-fx-background-color: rgb(10, 10, 170, 0.5);"
				+ "-fx-background-radius: 0.0px;"
				+ "-fx-border-width: 0.0px;");
		
		setListeners();
	}

	public void setListeners() {

		transitProbability.addListener( (observable, oldValue, newValue) -> {
			
			double transperancy = transitProbability.doubleValue();
			
			if(transperancy < 0.1) {
				transperancy = 0.2 - transperancy;
			
			} else {
				transperancy = 0.4 + transperancy;
			}
			
			
			
			this.setStyle("-fx-background-color: rgb(255,0,0, " + String.valueOf( 0.8 + transitProbability.doubleValue() ) + ");");
		});
	}

	private EventHandler<MouseEvent> drawHandler = new EventHandler<MouseEvent>() { 

		@Override 
		public void handle(MouseEvent e) {

			if(!doesRowExist()) {
				highlightCell(); 
			}
			e.consume();
		}
	};   

	public boolean doesRowExist() {

		for(Point point : controller.getGrid().getTransitPath()) {
			int y = point.getY();

			if(y == yCoordinate) {
				return true;
			}
		}
		return false; 
	}

	public void addDrawHandler() {
		this.addEventFilter(MouseEvent.MOUSE_EXITED, drawHandler);
	}

	public void removeDrawHandler() {
		this.removeEventFilter(MouseEvent.MOUSE_EXITED, drawHandler);
	}

	public void highlightCell() {
		this.setStyle("-fx-background-color: #FF0000;");
		count = count + 1;
	}
	
	public void highlightCell(String color) {
		this.setStyle("-fx-background-color: " + color + ";");
	}

	public int getX() {
		return xCoordinate;
	}

	public int getY() {
		return yCoordinate;
	}

	public TransitorCell incrementCount() {
		count = count + 1;
		return this;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void calculateTransitProbability(Integer total) {
		Double probability = count.doubleValue() / total.doubleValue(); 

		if(!Double.isNaN(probability)) {
			setTransitProbability( probability );
		} else {
			setTransitProbability( 0.0 );
		}
	}

	public void clearTransit() {
		count = 0;
		transitProbability.set(0.0);
		this.setStyle("-fx-background-color: rgb(10, 10, 170, 0.5);");
	}

	public Double getTransitProbability() {
		return transitProbability.getValue();
	}

	public void setTransitProbability(Double effectiveness) {
		this.transitProbability.set(effectiveness);
	}
}
