package com.drifft.mine.minefield;

public class Circle {

	private double radius = 15;

	public double getProbability(double distance) {		
		return 1.0 - Math.abs(distance / radius);
	}
	
	public double getRadius() {
		return radius;
	}
}
