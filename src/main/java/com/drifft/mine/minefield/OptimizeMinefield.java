package com.drifft.mine.minefield;

import java.util.ArrayList;
import java.util.List;

import com.drifft.mine.gui.Cell;
import com.drifft.mine.gui.Controller;

public class OptimizeMinefield {

	private int iterations = 1;
	private double biasX = 0.05;
	private double biasY = 0.05;
	
	private double coefficient = 3.0; 
	
	private Controller controller; 
	
	public OptimizeMinefield(Controller controller) {
		 this.controller = controller; 
	}
	
	private double getDistance(Cell iMine, Cell jMine) {
		
		int iX = iMine.getX();
		int iY = iMine.getY();

		int jX = jMine.getX();
		int jY = jMine.getY();

		return Math.sqrt( Math.pow(iX - jX, 2) + Math.pow(iY - jY, 2));	
	}

	private void calculateDerivative() {

		for(int i=0; i<controller.getGrid().getMineList().size(); i++) {	
			Cell iMine = controller.getGrid().getMineList().get(i);

			int iX = iMine.getX();
			int iY = iMine.getY();

			double iDerivativeX = 0.0;
			double iDerivativeY = 0.0;

			for(int j=0; j<controller.getGrid().getMineList().size(); j++) {
				Cell jMine = controller.getGrid().getMineList().get(j);	

				if(iMine != jMine) {

					int jX = jMine.getX();
					int jY = jMine.getY();

					iDerivativeX = iDerivativeX + ( -2 * (iX - jX) * 
							(coefficient * controller.getGrid().getCircle().getRadius() - getDistance(iMine, jMine)) ) / getDistance(iMine, jMine);

					iDerivativeY = iDerivativeY + ( -2 * (iY - jY) * 
							(coefficient * controller.getGrid().getCircle().getRadius() - getDistance(iMine, jMine)) ) / getDistance(iMine, jMine);
				}			
			}

			for(int k=0; k<controller.getGrid().getMineList().size(); k++) {
				Cell kMine = controller.getGrid().getMineList().get(k);	

				if(iMine != kMine) {

					int kX = kMine.getX();
					int kY = kMine.getY();

					iDerivativeX = iDerivativeX + ( 2 * (kX - iX) * 
							(coefficient * controller.getGrid().getCircle().getRadius() - getDistance(iMine, kMine)) ) / getDistance(iMine, kMine);

					iDerivativeY = iDerivativeY + ( 2 * (kY - iY) * 
							(coefficient * controller.getGrid().getCircle().getRadius() - getDistance(iMine, kMine)) ) / getDistance(iMine, kMine);
				}
			}

			double n = (controller.getGrid().getMineList().size() * controller.getGrid().getMineList().size());

			iDerivativeX = Math.floor(iDerivativeX / n); 
			((Cell) iMine).setDerivativeX(iDerivativeX); 

			iDerivativeY = Math.floor(iDerivativeY / n); 
			((Cell) iMine).setDerivativeY(iDerivativeY); 
		}
	}
	
	public void optimize() {

		for(int iter=0; iter<iterations; iter++) {
			calculateDerivative();
			controller.getGrid().clearMinefield();

			List<Cell> tmpList = new ArrayList<Cell>();

			for(int i=0; i<controller.getGrid().getMineList().size(); i++) {

				Cell iMine = controller.getGrid().getMineList().get(i);
				int iX = iMine.getX();
				int iY = iMine.getY();

				Double previousX = iMine.getDerivativeX();
				Double x = iX - biasX * previousX; 

				Double previousY = iMine.getDerivativeY();
				Double y = iY - biasY * previousY;

				Cell mine = controller.getGrid().getCellWithCoordinate(x.intValue(), y.intValue());

				if(mine != null) {
					tmpList.add(mine);
					controller.getGrid().highlightCell(mine);
				}
			}	
			controller.getGrid().getMineList().clear();
			controller.getGrid().setMineList(tmpList);
			controller.getGrid().evaluate();
		}
	}

	public double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(double coefficient) {
		this.coefficient = coefficient;
	}
}
