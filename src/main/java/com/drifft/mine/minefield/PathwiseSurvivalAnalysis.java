package com.drifft.mine.minefield;

import java.util.ArrayList;
import java.util.List;

import com.drifft.mine.gui.Controller;
import com.drifft.mine.gui.Menu.InitialPosition;
import com.drifft.mine.gui.Menu.Uncertanty;
import com.drifft.mine.utilities.Statistics;

public class PathwiseSurvivalAnalysis implements SurvivalAnalysis, Plottable {

	// Summary analysis
	private List<TimeCell> averageTimeValues;
	private List<HazardCell> averageStateValues; 
	private double expectedLifetime = 0.0;

	
	private int[] mean;
	private int[] sd; 

	// Chart data
	private Data pmfData = new Data();
	private Data cdfData = new Data();
	private Data survivalData = new Data();
	private Data hazardData = new Data();

	// Number of paths
	private final static Integer PATHS = 10000;

	private Controller controller; 

	public PathwiseSurvivalAnalysis(Controller controller) {
		this.controller = controller;
	}

	public void evaluateSurvival() {
		// User defined path 
		List<Point> userPath = controller.getGrid().getTransitPath(); 
		List<List<HazardCell>> hazardCollectionList = null; 

		InitialPosition initialPosition = controller.getMenu().getInitialPosition(); 	
		Uncertanty uncertanty = controller.getMenu().getUncertanty(); 

		if(userPath.get(0) == null) {
			userPath = new ArrayList<>(); 
			userPath.add(new Point(( controller.getColumns() / 2 ), 0)); 
		}
		
		for(int j=1; j<controller.getRows(); j++) {
					
			if(userPath.get(j) == null) {
				int x = userPath.get(j-1).getX();
				userPath.add(new Point(x, j));
			}
		}
		
		int x0 = userPath.get(0).getX(); 
		int bins = controller.getColumns();
		double[] initialPmf;
		List<List<Point>> pathList; 
		
		if(uncertanty == Uncertanty.NONE) {
			pathList             = generatePathsWithNoUncertanty(userPath); 
			hazardCollectionList = calculateSurvivalAnalysis(pathList); 
			
		} else if(uncertanty == Uncertanty.TIME_DEPENDENT) {

			switch(initialPosition) {

			case POINT:
				initialPmf           = Statistics.getUniformPMF(x0, 0, bins); 				
				pathList             = generateTimeDependentPaths(userPath, initialPmf);
				hazardCollectionList = calculateSurvivalAnalysis(pathList); 		
				break;

			case UNIFORM:
				int width = controller.getMenu().getDistributionWidth(); 

				initialPmf           = Statistics.getUniformPMF(x0, width, bins);
				pathList             = generateTimeDependentPaths(userPath, initialPmf);
				hazardCollectionList = calculateSurvivalAnalysis(pathList); 
				break;

			case GAUSSIAN:				
				int variance = controller.getMenu().getVariance();
				
				// gaussian
				
				initialPmf           = Statistics.getGaussianPMF(x0, variance, bins);
				pathList             = generateTimeDependentPaths(userPath, initialPmf);
				hazardCollectionList = calculateSurvivalAnalysis(pathList); 
				break;

			default:
				break;
			}

		}
		calculateAverageValuesInState(hazardCollectionList);
		calculateAverageValuesInTime(hazardCollectionList); 
		calculateExpectedLifetime();
		
		createSurfaceChartData();
	}

	// *** METHODS FOR A SINGLE PATH ***
	
	private List<HazardCell> calculatePathLifetimePmf(List<Point> path) {
		List<HazardCell> hazardCellList = new ArrayList<>(); 
		double[][] effectiveness = controller.getGrid().getEffectiveness();

		for(int j=0; j<path.size(); j++) {
			Point jPoint = path.get(j);
			int xj = jPoint.getX();
			int yj = jPoint.getY();

			double product = 1.0; 

			// Some points may fall outside the minefield
			try {
				product = product * effectiveness[xj][yj];

				for(int i=0; i<j; i++) {
					Point iPoint = path.get(i);
					int xi = iPoint.getX();
					int yi = iPoint.getY();

					product = product * ( 1.0 - effectiveness[xi][yi] ); 
				}
				// Set pmf
				HazardCell hazardCell = new HazardCell(xj, yj);  // getHazardCellByCoordinate(xj, yj);
				hazardCell.setLifetimePMF(product);
				hazardCellList.add(hazardCell);

			} catch (ArrayIndexOutOfBoundsException e) {
				// Ignore points out of bounds
			} finally {
				HazardCell hazardCell = new HazardCell(xj, yj);  // getHazardCellByCoordinate(xj, yj);
				hazardCell.setLifetimePMF(0.0);
				hazardCellList.add(hazardCell);
			}
		}
		return hazardCellList; 
	}

//	private void calculatePathLifetimeCdf(List<HazardCell> hazardCellList) {
//		double sum = 0.0; 
//
//		for(int j=0; j<hazardCellList.size(); j++) {
//			HazardCell jHazardCell = hazardCellList.get(j);
//			double product = jHazardCell.getLifetimePMF();
//
//			for(int i=0; i<j; i++) {				
//				HazardCell iHazardCell = hazardCellList.get(i);
//				product = product * ( 1.0 - iHazardCell.getLifetimePMF() );
//			}
//			sum = sum + product; 				
//			jHazardCell.setLifetimeCDF(sum);
//		}
//	}
	
	private void calculatePathLifetimeCdf(List<HazardCell> hazardCellList) {
	 
		for(int j=0; j<hazardCellList.size(); j++) {
			
			HazardCell jHazardCell = hazardCellList.get(j);
			double sum = 0.0; 

			for(int i=0; i<=j; i++) {				
				HazardCell iHazardCell = hazardCellList.get(i);
				sum = sum + iHazardCell.getLifetimePMF();
			}
			jHazardCell.setLifetimeCDF(sum);
		}
	}

	private void calculatePathSurvival(List<HazardCell> hazardCellList) {

		hazardCellList.get(0).setSurvival(1.0);
		
		for(int j=1; j<hazardCellList.size(); j++) {
			HazardCell jHazardCell = hazardCellList.get(j);
			HazardCell iHazardCell = hazardCellList.get(j-1);
			jHazardCell.setSurvival( 1.0 - iHazardCell.getLifetimeCDF());			
		}
	}

	private void calculatePathHazard(List<HazardCell> hazardCellList) {

		for(int j=0; j<hazardCellList.size(); j++) {
			HazardCell hazardCell = hazardCellList.get(j);
			hazardCell.setHazard( hazardCell.getLifetimePMF() / hazardCell.getSurvival() );
		}
	}
	
	// *** 

	private List<List<HazardCell>> calculateSurvivalAnalysis(List<List<Point>> collectionList) {
		List<List<HazardCell>> hazardCollectionList = new ArrayList<>();

		for(List<Point> generatedPath : collectionList) {
			List<HazardCell> hazardCellList = calculatePathLifetimePmf(generatedPath);
			calculatePathLifetimeCdf(hazardCellList); 
			calculatePathSurvival(hazardCellList);
			calculatePathHazard(hazardCellList);  
			hazardCollectionList.add(hazardCellList);
		}
		return hazardCollectionList;
	}

	private void calculateAverageValuesInState(List<List<HazardCell>> hazardCollectionList) {
		averageStateValues = new ArrayList<>(); 

		// Initialize each state
		for(int x=0; x<controller.getColumns(); x++) {

			for(int y=0; y<controller.getRows(); y++) {	
				averageStateValues.add(new HazardCell(x, y));				
			}
		}

		for(int x=0; x<controller.getColumns(); x++) {

			for(int y=0; y<controller.getRows(); y++) {
				double pmfSum = 0.0;
				double cdfSum = 0.0;
				double survivalSum = 0.0;
				double hazardSum = 0.0;
				Integer total = 0;

				for(List<HazardCell> hazardCellList : hazardCollectionList) {

					try {
						HazardCell hazardCell = getHazardCellByCoordinate(x, y, hazardCellList); 
						double pmf      = hazardCell.getLifetimePMF();
						double cdf      = hazardCell.getLifetimeCDF();
						double survival = hazardCell.getSurvival();
						double hazard   = hazardCell.getHazard();
						pmfSum = pmfSum + pmf;
						cdfSum = cdfSum + cdf;
						survivalSum = survivalSum + survival;
						hazardSum = hazardSum + hazard; 
						total = total + 1;
					} catch (NullPointerException e) {
						// not all cell exist
					}
				}
				HazardCell hazardCell = getHazardCellByCoordinate(x, y, averageStateValues); 

				if(total.doubleValue() > 0.0) {
					hazardCell.setLifetimePMF( pmfSum / total.doubleValue() );
					hazardCell.setLifetimeCDF( cdfSum / total.doubleValue() );
					hazardCell.setSurvival( survivalSum / total.doubleValue() );
					hazardCell.setHazard( hazardSum / total.doubleValue() );
				} 
			}
		}
	}

	private void calculateAverageValuesInTime(List<List<HazardCell>> hazardCollectionList) { 
		averageTimeValues = new ArrayList<>(); 

		for(int j=0; j<controller.getRows(); j++) {
			double pmfSum = 0.0;
			double cdfSum = 0.0;
			double survivalSum = 0.0;
			double hazardSum = 0.0;

			for(int n=0; n<hazardCollectionList.size(); n++) {

				HazardCell hazardCell = hazardCollectionList.get(n).get(j); 
				double pmf      = hazardCell.getLifetimePMF();
				double cdf      = hazardCell.getLifetimeCDF();
				double survival = hazardCell.getSurvival();
				double hazard   = hazardCell.getHazard();
				pmfSum = pmfSum + pmf;
				cdfSum = cdfSum + cdf;
				survivalSum = survivalSum + survival;
				hazardSum = hazardSum + hazard; 
			}
			Integer n = hazardCollectionList.size(); 
			TimeCell timeCell = new TimeCell(j);
			timeCell.setLifetimePMF( pmfSum / n.doubleValue() );
			timeCell.setLifetimeCDF( cdfSum / n.doubleValue() );
			timeCell.setSurvival( survivalSum / n.doubleValue() );
			timeCell.setHazard( hazardSum / n.doubleValue() );
			averageTimeValues.add(timeCell); 
		}
	}

	private void calculateExpectedLifetime() {	
		double sum = 0.0;

		for(int j=0; j<averageTimeValues.size(); j++) {
			sum = sum + averageTimeValues.get(j).getSurvival(); 
		}
		expectedLifetime = sum;
	}

	private HazardCell getHazardCellByCoordinate(int x, int y, List<HazardCell> hazardList) {

		for(HazardCell point : hazardList) {

			if(point.getX() == x && point.getY() == y) {
				return point;
			}
		}
		return null;
	}
	
	private List<List<Point>> generatePathsWithNoUncertanty(List<Point> meanPath) {
		List<List<Point>> collectionList = new ArrayList<>();
		List<Point> path = new ArrayList<>();
		
		for(int i=0; i<meanPath.size(); i++) {
			path.add(new Point(meanPath.get(i).getX(), meanPath.get(i).getY()));
		}
		collectionList.add(path);		
		return collectionList;
	}

	// *** TIME DEPENDENT UNCERTANTY ***
	private List<List<Point>> generateTimeDependentPaths(List<Point> meanPath, double[] initialPmf) {
		List<List<Point>> collectionList = new ArrayList<>();

		int steps = meanPath.size(); 	
		int[][] x = new int[PATHS][steps];
		int[] drift = new int[steps];
		int[][] diffusion = Statistics.getDiffusion(steps, PATHS);

		for(int j=0; j<steps-1; j++ ) {
			int xi = meanPath.get(j).getX(); 
			int xj = meanPath.get(j+1).getX();
			drift[j] = xj - xi;
		}

		for(int index = 0; index<diffusion.length; index++) {
			List<Point> path = new ArrayList<>();
			x[index][0] = Statistics.sampleDiscreetCustomDistribution(initialPmf);
			path.add(new Point(x[index][0], 0));

			for(int j=0; j<steps-1 ; j++) { 		
				x[index][j+1] = x[index][j] + drift[j] + diffusion[index][j];
				path.add(new Point(x[index][j+1], j+1));
			}
			collectionList.add(path);
		}
		mean = Statistics.calculateMean(x);
		sd = Statistics.calculateStandardDeviation(mean, x); 
		return collectionList; 
	}
	
	private void createSurfaceChartData() {
		pmfData.getData().clear();
		cdfData.getData().clear();
		survivalData.getData().clear();
		hazardData.getData().clear();

		for(int i=0; i<averageStateValues.size(); i++) {		
			int x = averageStateValues.get(i).getX();
			int y = averageStateValues.get(i).getY();

			double pmf = averageStateValues.get(i).getLifetimePMF();
			pmfData.addPoint(new DataPoint(i, x, y, pmf, pmf));

			double cdf = averageStateValues.get(i).getLifetimeCDF();
			cdfData.addPoint(new DataPoint(i, x, y, cdf, cdf));

			double survival = averageStateValues.get(i).getSurvival();
			survivalData.addPoint(new DataPoint(i, x, y, survival, survival));

			double hazard = averageStateValues.get(i).getHazard();
			hazardData.addPoint(new DataPoint(i, x, y, hazard, hazard));
		}	
	}

	public Data getPmfData() {
		return pmfData;
	}

	public Data getCdfData() {
		return cdfData;
	}

	public Data getSurvivalData() {
		return survivalData;
	}

	public Data getHazardData() {
		return hazardData;
	}
		
	public int[] getMean() {
		return mean;
	}

	public int[] getSd() {
		return sd;
	}
	
	@Override
	public double getMinefieldMean() {
		return controller.getGrid().getMean();
	}

	@Override
	public double getMinefieldVariance() {
		return controller.getGrid().getVariance();
	}	

	@Override
	public List<Double> getExpectedPmf() {
		List<Double> expectedPmf = new ArrayList<>();

		for(int i=0; i<averageTimeValues.size(); i++) {
			expectedPmf.add(averageTimeValues.get(i).getLifetimePMF());
		}
		return expectedPmf;
	}
	
	@Override
	public List<Double> getExpectedCdf() {
		List<Double> expectedCdf = new ArrayList<>();

		for(int i=0; i<averageTimeValues.size(); i++) {
			expectedCdf.add(averageTimeValues.get(i).getLifetimeCDF());
		}
		return expectedCdf;
	}

	@Override
	public List<Double> getExpectedSurvival() {
		List<Double> expectedSurvival = new ArrayList<>();

		for(int i=0; i<averageTimeValues.size(); i++) {
			expectedSurvival.add(averageTimeValues.get(i).getSurvival());
		}
		return expectedSurvival;
	}
	
	@Override
	public List<Double> getExpectedHazard() {
		List<Double> expectedHazard = new ArrayList<>();

		for(int i=0; i<averageTimeValues.size(); i++) {
			expectedHazard.add(averageTimeValues.get(i).getHazard());
		}
		return expectedHazard;
	}

	@Override
	public double getExpectedLifetime() {
		return expectedLifetime;
	}
}