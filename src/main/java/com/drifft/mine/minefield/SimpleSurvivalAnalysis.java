package com.drifft.mine.minefield;

import java.util.ArrayList;
import java.util.List;

import com.drifft.mine.gui.Controller;

public class SimpleSurvivalAnalysis implements SurvivalAnalysis, Plottable {

	private List<HazardCell> hazardList = new ArrayList<>();

	// Chart data
	private Data pmfData = new Data();
	private Data cdfData = new Data();
	private Data survivalData = new Data();
	private Data hazardData = new Data();
	
	private List<Double> expectedPmf;
	private List<Double> expectedCdf;
	private List<Double> expectedSurvival;
	private List<Double> expectedHazard;
	private double expectedLifetime = 0.0;

	private Controller controller;

	public SimpleSurvivalAnalysis(Controller controller) {
		this.controller = controller; 

		for(int y=0; y<controller.getRows(); y++) {

			for(int x=0; x<controller.getColumns(); x++) {
				hazardList.add(new HazardCell(x, y)); 
			}
		}
	}

	public HazardCell getHazardCellByCoordinate(int x, int y) {

		for(HazardCell point : hazardList) {

			if(point.getX() == x && point.getY() == y) {
				return point;
			}
		}
		return null;
	}

	public void evaluateSurvival() {
		double[][] effectiveness = controller.getGrid().getEffectiveness();
		calculateLifetimePmf(effectiveness); 
		calculateLifetimeCdf(); 
		calculateSurvival();
		calculateHazard();
		calculateExpectedValues(); 
		calculateExpectedLifetime();
		createChartData();
	}

	private void calculateLifetimePmf(double[][] effectiveness) {

		for(int x=0; x<controller.getColumns(); x++) {

			for(int yj=0; yj<controller.getRows(); yj++) {

				double product = 1.0; 
				product = product * effectiveness[x][yj]; 

				for(int yi=0; yi<yj; yi++) {
					product = product * ( 1.0 - effectiveness[x][yi] ); 
				}
				// Set pmf
				getHazardCellByCoordinate(x, yj).setLifetimePMF(product);	
			}
		}
	}

	private void calculateLifetimeCdf() {

		for(int x=0; x<controller.getColumns(); x++) {
			double sum = 0.0; 

			for(int yj=0; yj<controller.getRows(); yj++) {

				HazardCell yjHazardCell = getHazardCellByCoordinate(x, yj);
				double product = yjHazardCell.getLifetimePMF();

				for(int yi=0; yi<yj; yi++) {
					product = product * ( 1.0 - getHazardCellByCoordinate(x, yi).getLifetimePMF() );
				}
				sum = sum + product; 				
				yjHazardCell.setLifetimeCDF(sum);
			}
		}
	}

	private void calculateSurvival() {

		for(int x=0; x<controller.getColumns(); x++) {

			for(int y=0; y<controller.getRows(); y++) {
				HazardCell hazardCell = getHazardCellByCoordinate(x, y);
				hazardCell.setSurvival( 1.0 - hazardCell.getLifetimeCDF());
			}
		}
	}

	private void calculateHazard() {

		for(int x=0; x<controller.getColumns(); x++) {

			for(int y=0; y<controller.getRows(); y++) {
				HazardCell hazardCell = getHazardCellByCoordinate(x, y);
				
				double pmf = hazardCell.getLifetimePMF();
				double survival = hazardCell.getSurvival(); 
				double hazard = pmf / survival; 
			
				if(hazard == Double.NaN) {
					hazard = 0.0;
				}
				hazardCell.setHazard( hazard );
			}
		}
	}

	private void calculateExpectedValues() {
		expectedPmf = new ArrayList<>();
		expectedCdf = new ArrayList<>();
		expectedSurvival = new ArrayList<>();
		expectedHazard = new ArrayList<>();
		
		for(int y=0; y<controller.getRows(); y++) {
			double pmfSum = 0.0;
			double cdfSum = 0.0;
			double survivalSum = 0.0;
			double hazardSum = 0.0;
			
			for(int x=0; x<controller.getColumns(); x++) {
				HazardCell hazardCell = getHazardCellByCoordinate(x, y);
				pmfSum = pmfSum + hazardCell.getLifetimePMF();
				cdfSum = cdfSum + hazardCell.getLifetimeCDF();
				survivalSum = survivalSum + hazardCell.getSurvival();
				hazardSum = hazardSum + hazardCell.getHazard();
			}
			expectedPmf.add(      pmfSum      / controller.getColumns() );
			expectedCdf.add(      cdfSum      / controller.getColumns() );
			expectedSurvival.add( survivalSum / controller.getColumns() );
			expectedHazard.add(   hazardSum   / controller.getColumns() );
		}
	}
	
	private void calculateExpectedLifetime() {	
		double sum = 0.0;
		
		for(int i=0; i<expectedSurvival.size(); i++) {
			sum = sum + expectedSurvival.get(i); 
		}
		expectedLifetime = sum;
	}

	private void createChartData() {

		pmfData.getData().clear();
		cdfData.getData().clear();
		survivalData.getData().clear();
		hazardData.getData().clear();

		for(int i=0; i<hazardList.size(); i++) {

			int id = i+1;
			int x = hazardList.get(i).getX();
			int y = hazardList.get(i).getY();

			double pmf = hazardList.get(i).getLifetimePMF();
			pmfData.addPoint(new DataPoint(id, x, y, pmf, pmf));

			double cdf = hazardList.get(i).getLifetimeCDF();
			cdfData.addPoint(new DataPoint(id, x, y, cdf, cdf));

			double survival = hazardList.get(i).getSurvival();
			survivalData.addPoint(new DataPoint(id, x, y, survival, survival));

			double hazard = hazardList.get(i).getHazard();
			hazardData.addPoint(new DataPoint(id, x, y, hazard, hazard));
		}
	}

	// Getters and Setters

	public Data getPmfData() {
		return pmfData;
	}

	public Data getCdfData() {
		return cdfData;
	}

	public Data getSurvivalData() {
		return survivalData;
	}

	public Data getHazardData() {
		return hazardData;
	}

	public List<HazardCell> getHazardList() {
		return hazardList;
	}

	public double getExpectedLifetime() {
		return expectedLifetime;
	}

	@Override
	public List<Double> getExpectedPmf() {
		return expectedPmf;
	}

	@Override
	public List<Double> getExpectedCdf() {
		return expectedCdf;
	}
	
	@Override
	public List<Double> getExpectedSurvival() {
		return expectedSurvival;
	}

	@Override
	public List<Double> getExpectedHazard() {
		return expectedHazard;
	}

	@Override
	public double getMinefieldMean() {
		return controller.getGrid().getMean();
	}

	@Override
	public double getMinefieldVariance() {
		return controller.getGrid().getVariance();
	}	
}
