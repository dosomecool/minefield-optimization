package com.drifft.mine.minefield;

import java.util.List;

public interface SurvivalAnalysis {

	public double getMinefieldMean();
	public double getMinefieldVariance();
	public List<Double> getExpectedPmf();
	public List<Double> getExpectedCdf();
	public List<Double> getExpectedSurvival();
	public List<Double> getExpectedHazard();
	public double getExpectedLifetime();
}
