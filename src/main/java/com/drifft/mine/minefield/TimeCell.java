package com.drifft.mine.minefield;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TimeCell {

	private int step; 

	private double lifetimePMF = Double.valueOf(0.0);
	private double lifetimeCDF = Double.valueOf(0.0);
	private double survival = Double.valueOf(0.0);
	private double hazard = Double.valueOf(0.0);

	public TimeCell(int step) {
		this.step = step;
	}

	// Getters and Setters
	
	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public double getLifetimePMF() {
		return lifetimePMF;
	}

	public void setLifetimePMF(Double lifetimePMF) {
		this.lifetimePMF = lifetimePMF;
	}

	public double getLifetimeCDF() {
		return lifetimeCDF;
	}

	public void setLifetimeCDF(Double lifetimeCDF) {
		this.lifetimeCDF = lifetimeCDF;
	}

	public double getSurvival() {
		return survival;
	}

	public void setSurvival(Double survival) {
		this.survival = survival;
	}

	public double getHazard() {
		return hazard;
	}

	public void setHazard(Double hazard) {
		this.hazard = hazard;
	}
	
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("step: ").append(step);
		sb.append("\nlifetimePMF: ").append(lifetimePMF);
		sb.append("\nlifetimeCDF: ").append(lifetimeCDF);
		sb.append("\nsurvival: ").append(survival);
		sb.append("\nhazard: ").append(hazard);

		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
