package com.drifft.mine.utilities;

import java.util.Random;

public class Statistics {

	public static double[][] fillDoubleArray(double value, int maxColumns, int maxRows) {
		double[][] array = new double[maxColumns][maxRows];

		for(int x=0; x<maxColumns; x++) {

			for(int y=0; y<maxRows; y++) {
				array[x][y] = value; 
			}
		}
		return array;
	}

	// Poisson random variable
	public static int getPoisson(double lambda) {
		double L = Math.exp(-lambda);
		double p = 1.0;
		int k = 0;

		do {
			k++;
			p *= Math.random();
		} while (p > L);

		return k - 1;
	}

	// Binomial random variable
	public static int getBinomial(int n, double p) {
		int x = 0;

		for(int i = 0; i < n; i++) {
			if(Math.random() < p)
				x++;
		}
		return x;
	}

	public static int sampleDiscreetCustomDistribution(double[] pmf) {
		Random random = new Random(); 
		double u = random.nextDouble();

		if(u <= pmf[0]) {
			return 0; 
		}

		for(int state=1; state<pmf.length; state++) {
			double leftSum = 0.0;
			double rightSum = 0.0;

			for(int i=0; i<=state-1; i++) {
				leftSum = leftSum + pmf[i];
			}

			for(int i=0; i<=state; i++) {
				rightSum = rightSum + pmf[i];
			}

			if(leftSum < u && u <= rightSum) {
				return state; 
			}
		}
		return 0; 
	}

	// Source: http://www.nrbook.com/devroye/Devroye_files/chapter_three.pdf 
	// Pr[X=i]=c*exp[(|x| + 0.5)^2 / 2*s^2]
	public static double[] getGaussianPMF(int mean, double variance, int width) {

		if(width <=0) {
			throw new IllegalArgumentException("The Gaussian distribution width must have at least one bin!");
		}

		int bins = 0; 

		if((width % 2) == 1) { 
			bins = width + 1;
		} else {
			bins = width; 
		}

		double[] temp = new double[bins]; 
		double[] tempPmf = new double[bins]; 
		double[] pmf = new double[bins]; 
		double c = 0.0;

		for(int i=0; i<( bins / 2 ); i++) {
			double a = Math.pow(i + 0.5 , 2); 
			double b = 2.0 * Math.pow(variance, 2); 
			temp[i] = Math.exp( a / b ); 
		}

		for(int j=( bins-1 ); j>=( bins / 2 ); j--) {
			int i = (bins-1) - j; 
			temp[j] = temp[i];
		}

		for(int i=0; i<bins; i++) {
			c = c + temp[i];
		}

		for(int i=0; i<bins; i++) {
			tempPmf[i] = temp[i] / c;
		}

		int distance = ( bins / 2 ) - mean;	
		
		for (int i=0; i<bins; i++) {
			
			int shift = i + distance; 
			
			if(shift>=0 && shift<bins) {
				pmf[i] = tempPmf[shift];  
			} else {
				pmf[i] = 0.0;
			}
		}
		return pmf;
	}

	public static double[] getUniformPMF(int center, int width, int bins) {
		double[] pmf = new double[bins]; 
		
		if(width > bins) {
			throw new IllegalArgumentException("Uniform distribution width must less or equal to the bins!");
		}
		
		int remainder = width % 2; 
		int a = center - ( width / 2 );
		int b = center + ( width / 2 ) + remainder;
		Integer n = 1 + b - a;  
		
		for(int i=0; i<bins; i++) {
			
			if(i>=a && i<=b) {
				pmf[i] = 1.0 / n.doubleValue();
				
			} else {
				pmf[i] = 0.0;
			}
		}
		return pmf;
	}

	public static int[][] getDiffusion(int steps, int paths) {
		int[][] diffusion = new int[paths][steps]; 
		double[] pmf = { 0.01, 0.98, 0.01 }; 

		for(int index = 0; index<paths; index++) { 
			diffusion[index][0] = 0;

			for(int j=0; j<steps-1 ; j++) { 

				int delta = 0;
				try {
					delta = Statistics.sampleDiscreetCustomDistribution(pmf) - 1;

				} catch (NullPointerException n) {

				}
				diffusion[index][j+1] = diffusion[index][j] + delta;
			}  
		}
		return diffusion; 
	}
	
	public static int[] calculateMean(int[][] data) {
		int dim0 = data.length;
		int dim1 = data[0].length;
		int[] mean = new int[dim1]; 
		
		for(int j=0; j<dim1; j++) {
			double sum = 0.0;
			
			for(int i=0; i<dim0; i++) {
				sum = sum + data[i][j];
			}
			mean[j] = (int) ( sum / dim0 );
			
		}
		return mean;
	}
	
	public static int[] calculateStandardDeviation(int[] mean, int[][] data) {
		int dim0 = data.length;
		int dim1 = data[0].length;
		int[] sd = new int[dim1]; 
		
		for(int j=0; j<dim1; j++) {
			double sum = 0;
			
			for(int i=0; i<dim0; i++) {
				sum = (sum + Math.pow(data[i][j]-mean[j], 2));
			}
			sd[j] = (int) Math.sqrt( sum / dim0 );
		}
		return sd;
	}

	public static double calculateMean(double[] data) {	
		double sum = 0.0;

		for(int i=0; i<data.length; i++) { 
			sum = sum + data[i];
		}
		return sum / Double.valueOf( data.length ); 
	}

	public static double calculateVariance(double mean, double[] data) {
		double sum = 0.0;

		for(int i=0; i<data.length; i++) { 
			sum = sum + Math.pow(data[i]-mean, 2);
		}
		return sum / Double.valueOf( data.length ); 
	}

	public static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	public static int argmin(double[] a) {
		double min = Double.POSITIVE_INFINITY;
		int argmin = 0;

		for (int i = 0; i < a.length; i++) {

			if (a[i] < min) {
				min = a[i];
				argmin = i;
			}
		}
		return argmin;
	}

	public static int argmax(float[] a) {
		float max = Float.NEGATIVE_INFINITY;
		int argmax = 0;

		for (int i = 0; i < a.length; i++) {
			if (a[i] > max) {
				max = a[i];
				argmax = i;
			}
		}
		return argmax;
	}
}
